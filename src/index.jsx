import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';
import './sass/style.scss';

ReactDOM.render(<App />, document.getElementById('app'));