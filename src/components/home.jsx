import React, { Component , Fragment } from 'react';
import ReactDOM from 'react-dom';
import DocumentTitle  from  'react-document-title';

class Home extends Component {
	   render() {
      return (
      	<DocumentTitle title="homepage">
      		<div>
      			<h1 className="text-center">ini home page</h1>
      		</div>	
      	</DocumentTitle>
      	)

  }
}

  export default Home;