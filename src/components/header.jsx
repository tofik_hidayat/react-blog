import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
class Header extends Component {
   render() {
      return (
         <div className="parent-header">
          	<div className="menu-nav"> 
          		<nav  className="navbar">
          			<ul className="menu-parent">

          				<li className="menu-item"><Link to="/">Home</Link> </li> 
          				<li className="menu-item"><Link to="/about">About</Link> </li> 
          				<li className="menu-item"><Link to="/blog">blog</Link>  </li> 
                  
          			</ul>
          		</nav> 
          	</div>
         </div>
      );
   }
}

export default Header;