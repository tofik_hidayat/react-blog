import React, { Component , Fragment } from 'react';
import ReactDOM from 'react-dom';
import DocumentTitle  from  'react-document-title';

class  Error extends Component {
	   render() {
      return (
      	<DocumentTitle title="404">
	      	<div>
		      	<h1 className="text-center">404</h1>
		      	<h2 className="text-center">oops page not found</h2>
	      	</div>
      	</DocumentTitle>
      	)

  }
}

export default Error;