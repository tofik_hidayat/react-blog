import React, { Component , Fragment } from 'react';
import ReactDOM from 'react-dom';
import { Link , BrowserRouter as Router , Route , Switch  } from 'react-router-dom';
import DocumentTitle  from  'react-document-title';
import Header from './components/header.jsx';
import Home from './components/home.jsx';
import About from './components/about.jsx';
import NoMatch from './components/404.jsx';

class App extends Component {
  render() {
       return(
     <Router>
     	<Fragment>
	   <Header />
	     <Switch>
	      <Route exact path="/" component={Home} />
	      <Route exact path="/about" component={About} />
	      <Route exact path="*" component={NoMatch} />
	     </Switch>
     	</Fragment>
     	
    </Router>

       	);
  }
}

export default App;