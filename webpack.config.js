const env = require('dotenv');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
env.load();
module.exports = {
    /* ... */
    entry: './src/index.jsx',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'app.js',
        publicPath: '/'
    },
    module: {
        rules: [{
            test: /\.jsx$/,
            exclude: /node_modules/,
            use: {
                loader: "babel-loader", 
            }
        }, {
            test: /\.scss$/,
            use: ['style-loader', 'css-loader', 'sass-loader']
        }, {
            test: /\.(jpe?g|gif|png|eot|svg|woff2|ttf)$/,
            loader: 'file'
        }]
    },
    devServer: {
        inline: true,
        compress: true,
        //host:process.env.HOST,
        //port: process.env.PORT,
        //https: process.env.SSL,
        //port: 80,
         disableHostCheck: true,
        overlay: {
            //warnings: true,
            errors: true,
        },
        historyApiFallback: {
            disableDotRule: true
        },

    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            filename: "./index.html"
        })
    ]
}